# ElenQ Technology Website

This is ElenQ Technology's website code.

It's created using [Schiumato][schiumato], a Static Website Generator developed
by ElenQ Technology.

To create this site just run:

```
schiumato create
```

The default arguments should be enough.


## File structure

- `templates` folder contains the `nunjucks` templates which define the shape
  of the final HTML files.

- `locales` folder contains all the translations of the website.

- `static` folder contains all the static files which are directly copied with
  no processing. CSS, JavaScript, Images...

For more information check [Shiumato's Documentation][schiumato].


## Licensing

All the **content** of the website is released under [Creative Commons
Attribution-Share Alike][cc-by-sa].

The code and the HTML templates are released under MIT license.

### External modules

This code makes use of projects developed by third parties. This is the list:

- LatoLatinWeb: Font. Distributed under SIL Open Font License. Copyright Łukasz
  Dziedzic.

[schiumato]: https://gitlab.com/ElenQ/schiumato
[cc-by-sa]:  https://creativecommons.org/licenses/by-sa/4.0/
